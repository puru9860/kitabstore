﻿using Microsoft.AspNetCore.Identity;
using System.Web.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Models
{
    public class Order
    {
        
        [Key]
        public int OrderId { get; set; }
        [Required(ErrorMessage = "Please enter your name")]
        [Display(Name = "Name")]
        [StringLength(50)]
        public string Name { get; set; }

        
        [Required(ErrorMessage = "Please enter your address")]
        [StringLength(100)]
        [Display(Name = "Address")]
        public string Address { get; set; }
        
        [Required(ErrorMessage = "Please enter your phone number")]
        [StringLength(25)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
        

        [DisplayName("Order Total")]
        public decimal OrderTotal { get; set; }

        
        [DisplayName("Order Date")]
        public DateTime OrderPlaced { get; set; }
        
        public bool OrderStatus { get; set; }

        public string UserId { get; set; }
        

        public virtual ICollection<Books> Books { get; set; }


        public Order()
        {
            Books = new HashSet<Books>();
        }
    }
}
