﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookStore.Models.ViewModels
{
    public class BookCatViewModel
    {
        public Books Books { get; set; }
        public Categories Categories { get; set; }
    }
}