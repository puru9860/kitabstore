namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updated2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Books", "CategoriesId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "CategoriesId", c => c.Int(nullable: false));
        }
    }
}
