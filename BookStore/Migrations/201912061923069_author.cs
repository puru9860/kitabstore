namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class author : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        AuthorId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.AuthorId);
            
            CreateTable(
                "dbo.AuthorBooks",
                c => new
                    {
                        Author_AuthorId = c.Int(nullable: false),
                        Books_BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Author_AuthorId, t.Books_BookId })
                .ForeignKey("dbo.Authors", t => t.Author_AuthorId, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Books_BookId, cascadeDelete: true)
                .Index(t => t.Author_AuthorId)
                .Index(t => t.Books_BookId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AuthorBooks", "Books_BookId", "dbo.Books");
            DropForeignKey("dbo.AuthorBooks", "Author_AuthorId", "dbo.Authors");
            DropIndex("dbo.AuthorBooks", new[] { "Books_BookId" });
            DropIndex("dbo.AuthorBooks", new[] { "Author_AuthorId" });
            DropTable("dbo.AuthorBooks");
            DropTable("dbo.Authors");
        }
    }
}
