namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rev : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ShoppingCartItems", "Book_BookId", "dbo.Books");
            DropIndex("dbo.ShoppingCartItems", new[] { "Book_BookId" });
            AlterColumn("dbo.Orders", "OrderStatus", c => c.Boolean(nullable: false));
            DropTable("dbo.ShoppingCartItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ShoppingCartItems",
                c => new
                    {
                        ShoppingCartItemId = c.Int(nullable: false, identity: true),
                        ShoppingCartId = c.String(),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Book_BookId = c.Int(),
                    })
                .PrimaryKey(t => t.ShoppingCartItemId);
            
            AlterColumn("dbo.Orders", "OrderStatus", c => c.String());
            CreateIndex("dbo.ShoppingCartItems", "Book_BookId");
            AddForeignKey("dbo.ShoppingCartItems", "Book_BookId", "dbo.Books", "BookId");
        }
    }
}
