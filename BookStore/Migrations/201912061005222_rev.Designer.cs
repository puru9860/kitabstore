// <auto-generated />
namespace BookStore.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class rev : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(rev));
        
        string IMigrationMetadata.Id
        {
            get { return "201912061005222_rev"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
