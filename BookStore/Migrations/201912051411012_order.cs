namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class order : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Order_OrderId", c => c.Int());
            AddColumn("dbo.Orders", "OrderStatus", c => c.String());
            AlterColumn("dbo.Orders", "UserId", c => c.String());
            CreateIndex("dbo.Books", "Order_OrderId");
            AddForeignKey("dbo.Books", "Order_OrderId", "dbo.Orders", "OrderId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Books", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.Books", new[] { "Order_OrderId" });
            AlterColumn("dbo.Orders", "UserId", c => c.Int(nullable: false));
            DropColumn("dbo.Orders", "OrderStatus");
            DropColumn("dbo.Books", "Order_OrderId");
        }
    }
}
