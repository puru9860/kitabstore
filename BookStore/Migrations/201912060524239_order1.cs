namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class order1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Books", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.Books", new[] { "Order_OrderId" });
            CreateTable(
                "dbo.OrderBooks",
                c => new
                    {
                        Order_OrderId = c.Int(nullable: false),
                        Books_BookId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Order_OrderId, t.Books_BookId })
                .ForeignKey("dbo.Orders", t => t.Order_OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Books_BookId, cascadeDelete: true)
                .Index(t => t.Order_OrderId)
                .Index(t => t.Books_BookId);
            
            DropColumn("dbo.Books", "Order_OrderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "Order_OrderId", c => c.Int());
            DropForeignKey("dbo.OrderBooks", "Books_BookId", "dbo.Books");
            DropForeignKey("dbo.OrderBooks", "Order_OrderId", "dbo.Orders");
            DropIndex("dbo.OrderBooks", new[] { "Books_BookId" });
            DropIndex("dbo.OrderBooks", new[] { "Order_OrderId" });
            DropTable("dbo.OrderBooks");
            CreateIndex("dbo.Books", "Order_OrderId");
            AddForeignKey("dbo.Books", "Order_OrderId", "dbo.Orders", "OrderId");
        }
    }
}
