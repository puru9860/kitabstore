namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tetis : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ShoppingCartItems", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ShoppingCartItems", "Amount", c => c.Int(nullable: false));
        }
    }
}
