namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orders : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "Name", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.Orders", "FirstName");
            DropColumn("dbo.Orders", "LastName");
            DropColumn("dbo.Orders", "City");
            DropColumn("dbo.Orders", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Orders", "Email", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Orders", "City", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Orders", "LastName", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Orders", "FirstName", c => c.String(nullable: false, maxLength: 50));
            DropColumn("dbo.Orders", "Name");
        }
    }
}
