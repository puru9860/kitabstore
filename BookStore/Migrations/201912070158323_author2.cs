namespace BookStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class author2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AuthorBooks", newName: "BooksAuthors");
            DropPrimaryKey("dbo.BooksAuthors");
            AddPrimaryKey("dbo.BooksAuthors", new[] { "Books_BookId", "Author_AuthorId" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.BooksAuthors");
            AddPrimaryKey("dbo.BooksAuthors", new[] { "Author_AuthorId", "Books_BookId" });
            RenameTable(name: "dbo.BooksAuthors", newName: "AuthorBooks");
        }
    }
}
