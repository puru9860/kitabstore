﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.IO;
using System.Web.Mvc;
using BookStore.Models;
using System.Configuration;

namespace BookStore.Controllers
{
    public class BooksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Books
        public ActionResult Index(string searchString)
        {
            var books = from b in db.Books
                        select b;
            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(s => s.Name.Contains(searchString));
                
            }
            return View(books.ToList());
        }
        public ActionResult UserView(string searchString,string heading)
        {
            ViewBag.heading = heading;
            var books = from b in db.Books.Include(b => b.Category)
                        .Include(b=>b.Author)
                        select b;
            if (!String.IsNullOrEmpty(searchString))
            {
                books = books.Where(s => s.Name.Contains(searchString));
                ViewBag.heading = "Results: ";
            }
            return View(books.ToList());
        }
        // GET: Books/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Books books = db.Books.Find(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            return View(books);
        }

        [Authorize(Roles = "Admin")]
        // GET: Books/Create
        public ActionResult Create()
        {
            
            ViewBag.CategoriesList= new MultiSelectList(db.Categories, "CategoryId","Name");
            return View();
        }

        [Authorize(Roles = "Admin")]
        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BookId,Name,Price,Description,Authors,ImageFile,CategoriesId")] Books books)
        {
            if (ModelState.IsValid)
            { 
                string FileName = Path.GetFileNameWithoutExtension(books.ImageFile.FileName);
                
                string FileExtension = Path.GetExtension(books.ImageFile.FileName);
                
                FileName = DateTime.Now.ToString("yyyyMMdd") + "-" + FileName.Trim() + FileExtension;
                
                books.ImageUrl = "~/BookImage/"+ FileName;

                FileName = Path.Combine(Server.MapPath("~/BookImage/"),FileName);
 
               
                books.ImageFile.SaveAs(FileName);
                Author author = new Author();
                for(int i=0; i< books.Authors.Count();i++)
               
                {
                    author.Name = books.Authors[i];
                    db.Authors.Add(author);
                    db.SaveChanges();
                    books.Author.Add(author);
                }
                

                foreach (var id in books.CategoriesId)
                {
                    books.Category.Add(db.Categories.Find(id));
                }
                db.Books.Add(books);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(books);
        }
        [Authorize(Roles = "Admin")]

        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Books books = db.Books.Find(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            return View(books);
        }

        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BookId,Name,Price,Description,ImageUrl,CategoriesId")] Books books)
        {
            if (ModelState.IsValid)
            {
                db.Entry(books).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(books);
        }

        [Authorize(Roles = "Admin")]

        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Books books = db.Books.Find(id);
            if (books == null)
            {
                return HttpNotFound();
            }
            return View(books);
        }

        [Authorize(Roles = "Admin")]
        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Books books = db.Books.Find(id);
            db.Books.Remove(books);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
