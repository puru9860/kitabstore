﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using BookStore.Models;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace BookStore.Controllers
{
    [Authorize(Roles = "Admin, User")]
    public class CartController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Cart
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Soon()
        {
            return View();
        }
        public ActionResult OrderNow(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (Session["cart"] == null)
            {
                List<Books> cartlist = new List<Books>();
                Books book = db.Books.Find(id);
                cartlist.Add(book);
                Session["cart"] = cartlist;
            }
            else
            {
                List<Books> cartlist = (List<Books>)Session["cart"];
                Books book = db.Books.Find(id);
                cartlist.Add(book);
                Session["cart"] = cartlist;
            }

            return RedirectToAction("Index");
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (Session["cart"] == null)
            {
                return RedirectToAction("Index");
            }
            List<Books> cartlist = (List<Books>)Session["cart"];
            foreach(var item in cartlist.ToList())
            {
                if(item.BookId==id)
                {
                    cartlist.Remove(item);
                }
            }
            Session["cart"] = cartlist;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "User")]
        public ActionResult Checkout()
        {
            if (Session["cart"] == null)
            {
                return View("Index");
            }

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Checkout([Bind(Include = "Name,Address,City,PhoneNumber,Email")]Order order)
        {
            
            if (ModelState.IsValid)
            {
                if(order==null)
                {
                    return View("Index");
                }
                decimal Total = 0;
                List<Books> cartlist = (List<Books>)Session["cart"];
                foreach (Books item in cartlist)
                {
                    order.Books.Add(db.Books.Find(item.BookId));
                    Total += item.Price;
                }
                order.OrderTotal = Total;
                order.OrderPlaced = DateTime.Now;
                order.OrderStatus = false;
                order.UserId = User.Identity.GetUserId();

                db.Orders.Add(order);
                db.SaveChanges();
                Session["cart"]=null;
                return View("soon");
                
            }
            return View(order);
        }

    }
}